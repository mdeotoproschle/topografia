# Topografia

## ¿En qué consiste este script?

1. Descarga información de topografia *NASA Shuttle Radar Topography Mission Global 1 arc second NetCDF* desde el repositorio de [EarthData](https://search.earthdata.nasa.gov/search?q=SRTMGL1%20NC) para ello se debe crear un [usuario](https://www.earthdata.nasa.gov/eosdis/science-system-description/eosdis-components/earthdata-login). El usuario y la contraseña le serán requeridos en el script, particularmente en el Box [14] *Funcion de descarga desde repositorio EARTHDATA* en donde dice usuario se debera poner su *usuario* y donde dice *contraseña* se deberá poner su contraseña.

2. Utiliza un archivo *kmz* que se descarga desde la página de [Parques Nacionales](https://sib.gob.ar/institucional/mapas-google). El ejemplo es del [Parque Nacional Nahuel Huapi](https://www.nahuelhuapi.gov.ar/).

3. Corta el archivo de topografía mediante el poligono del parque nacional, calcula las estadisticas y realiza 2 figuras:
    # ![Topografia del parque nacional](https://gitlab.com/mdeotoproschle/topografia/-/blob/main/Figuras/topografia.png)

    # ![Histograma de alturas del parque nacional](https://gitlab.com/mdeotoproschle/topografia/-/blob/main/Figuras/histograma.png)

### Ejecución

Sube el archivo **.ipynb** a tu Nube de Google y ejecutalo todo de una vez o paso por paso, para ello les dejo las siguientes instrucciones:
instrucciones:

    1. Bajen el archivo “.ipynb” del git a sus computadoras (para bajarlo, apreten el boton derecho del mouse sobre el link, y elijan la opción para bajar el archivo)

    2. Vayan a https://colab.research.google.com

    3. Vayan a la pestaña “Subir (Upload)” y suban el archivo “.ipynb”.

# Nota

El script puede ser reproducible a otros Parques Nacionales u otras regiones del mundo, siempre y cuando se disponga del archivo de contornos de la región (archivo *.shp* o *.kmz*). Por ejemplo el archivo de contornos de parques nacionales descargado presenta otros parques nacionales, pero en este ejemplo se seleccionó el Nahuel Huapi. Ver en el Box [10] *# Selecciono parque*.
